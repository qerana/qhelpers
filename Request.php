<?php

/*
 * This file is part of keranaProject
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');

/**
 * -----------------------------------------------------------------------------
 * ClassRequest
 * ------------------------------------------------------------------------------
 * Access an validate var type, from post or get request
 * @author diemarc
 */
class Request
{

    public static

    /** @var string, the current method request */
            $request_method,
            /** @array, super global $_REQUEST rewrite */
            $request;

    /**
     * -------------------------------------------------------------------------
     * Start the request method, an sanitize all values for a request
     * -------------------------------------------------------------------------
     */
    public static function init()
    {
        self::$request_method = (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') ? INPUT_POST : INPUT_GET;
        self::$request = filter_input_array(self::$request_method, FILTER_SANITIZE_SPECIAL_CHARS);
    }

    /**
     * -------------------------------------------------------------------------
     * Get field via post or get
     * -------------------------------------------------------------------------
     * @param string $field
     * @return type
     */
    public static function get(string $field)
    {

        self::init();
        return filter_var(self::$request[$field]);
    }

    /**
     * Get value 
     * @param string $field
     * @param type $var
     * @param type $filter
     * @param type $remove_whitespace
     * @return type
     */
    public static function getValue(string $field, $var = '', $filter = FILTER_SANITIZE_NUMBER_INT, $remove_whitespace = false)
    {

        if (!empty($var)) {
            $clean_string =  filter_var($var, $filter);
        } else {
            self::init();
            $clean_string = filter_var(self::$request[$field], $filter);
        }
        // remove all tabs
        $clean_string = ($remove_whitespace) ? preg_replace('/\s+/', '', $clean_string) : $clean_string;
        
        return trim($clean_string);
    }

    /**
     * -----------------------------------------------------------------------------
     * Get form data, recolect all data send by a form
     * -----------------------------------------------------------------------------
     * @param bool $only_setted , true= only setted values
     * @param type $prefix , prefix of name form
     * @return array
     */
    public static function getFormData(bool $only_setted = true, $prefix = 'f_'): array
    {

        self::init();
        $data = [];

        $prefix_len = strlen($prefix);

        foreach (self::$request AS $k => $v):

            if ($only_setted) {
                ($v != '') ? (substr($k, 0, $prefix_len) == $prefix) ? $data[substr($k, $prefix_len)] = $v : '' : '';
            } else {
                (substr($k, 0, $prefix_len) == $prefix) ? $data[substr($k, $prefix_len)] = $v : '';
            }

        endforeach;

        return $data;
    }

}
