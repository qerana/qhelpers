<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

/**
 * *****************************************************************************
 * Description of Number
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class Number
{

    /**
     * -------------------------------------------------------------------------
     * get spanish format
     * -------------------------------------------------------------------------
     * @param type $number
     * @param type $decimal
     * @return type
     */
    public static function toString($number, $decimal = 2)
    {
        return number_format($number, $decimal, ',', '.');
    }


    /**
     * -------------------------------------------------------------------------
     * Validate if  param_values is a valid number
     * -------------------------------------------------------------------------
     * @param mixed $float
     * @return number
     */
    public static function toFloat($float)
    {

        // if is not a float and is not numeric, try to formated 
        if (!filter_var($float, FILTER_VALIDATE_FLOAT) AND ( is_numeric($float + 1)) AND $float != 0.0) {

            $fmt = new \NumberFormatter('de_DE', \NumberFormatter::DECIMAL);
            $num_formatted = $fmt->parse($float);

            // if parsing return empty value, show error
            if (!$num_formatted) {
                throw new \InvalidArgumentException('Float invalid');
            } else {
                return $num_formatted;
            }
            // if not a number
        } else if (!is_numeric($float)) {
            throw new \InvalidArgumentException('NAN');
        } else {

            // if the value format is like to 9.8 return this value 
            return $float;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Check number
     * -------------------------------------------------------------------------
     * @param type $number
     * @param type $length
     * @throws \InvalidArgumentException
     */
    public static function checkInt($number,$length = 0){
        
        
        if(filter_var($number,FILTER_VALIDATE_INT) === false){
            throw new \InvalidArgumentException($number.' is not numeric');
        }else if($length > 0){
            
            $length_number = strlen($number);
            if($length != $length_number){
                throw  new  \InvalidArgumentException($number.' must have '.$length.' positions');
            }
            
        }
        
        return $number;
    }
}
