<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

/**
 * *****************************************************************************
 * Description of Date helper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class Date
{


    /**
     * -------------------------------------------------------------------------
     * Convert date to a format
     * -------------------------------------------------------------------------
     * @param type $date
     * @param type $format
     * @return type
     */
    public static function convertDate($date, $format = 'd-m-Y')
    {

        $Date = date_create($date);
        if(!is_object($Date)){
            throw new \InvalidArgumentException('Invalid date ');
        }
        
        return date_format($Date, $format);
    }

    /**
     * -------------------------------------------------------------------------
     * format a date to db compatibility
     * -------------------------------------------------------------------------
     * @param type $date
     * @return type
     */
    public static function toDate($date)
    {
        return self::convertDate($date, 'Y-m-d H:i:s');
    }
    
      /**
     * -------------------------------------------------------------------------
     * Convierte fechas ingles a castellano
     * -------------------------------------------------------------------------
     * @param type $timestamp
     * @param type $format
     * @return type
     */
    public static function toEngDate($date1 = '')
    {
        if ($date1 !== '') {
            $date = str_replace('/', '-', $date1);
            $myStringDate = strtotime($date);
            $date_formatted = date('Y-m-d', $myStringDate);
        }else{
            $date_formatted = '0000-00-00 00:00:00';
        }



        return $date_formatted;
    }



    /**
     * -------------------------------------------------------------------------
     * 
     * -------------------------------------------------------------------------
     * @param type $date
     * @return type
     */
    public static function toString($date,$format = 'd-m-Y')
    {
        return self::convertDate($date,$format);
    }

       /**
     *  Get the diff between 2 dates
     *   @param type $date1
     *   @param type $date2
     *   @param string $format , output format
         @url: https://www.php.net/manual/en/dateinterval.format.php 
     */
    public static function diffDate($date1, $date2, $format = '%R%a')
    {

        $Date1 = new \DateTime($date1);
        $Date2 = new \DateTime($date2);
        $Diff = $Date1->diff($Date2);

        return $Diff->format($format);
    }

     /**
     * Devuelve un array de meses
     * @return type
     */
    public static function getMonthRange()
    {

        return [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        ];
    }

}
