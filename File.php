<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

/**
 * *****************************************************************************
 * Description of File
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class File
{

    /**
     * -------------------------------------------------------------------------
     * Create a file
     * -------------------------------------------------------------------------
     * @param string $full_path_file
     */
    public static function createFile(string $full_path_file)
    {

        try {
            fopen($full_path_file, 'w');
        } catch (Exception $ex) {
            \Qerana\Exceptions::ShowException('Fail to create File', $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Add new line in content
     * -------------------------------------------------------------------------
     * @param type $content
     * @param type $insert_code
     * @return type
     */
    public static function addNewLineInFile($content, $insert_code, $reference = '')
    {
        // count number of lines
        $lines = explode("\n", $content);
        $lines_content = array_map('trim', $lines);

        if ($reference != '') {
            $position_to_break = array_search($reference, $lines_content);
            $line_number = $position_to_break + 2;
        } else {
            $line_number = count($lines) - 2;
        }


        // extract the line_number for class
        $segment_1 = array_slice($lines, 0, $line_number);

        // extract the last -1 line of ocde
        $segment_2 = array_slice($lines, $line_number, count($lines) - 1);

        // add before the segment1 the new code
        array_push($segment_1, $insert_code);

        // put together
        $new_content = array_merge($segment_1, $segment_2);
        return $str = implode("\n", $new_content);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new line in content
     * -------------------------------------------------------------------------
     * @param type $content
     * @param type $insert_code
     * @return type
     */
    public static function addNewLineBeforeLine($content, $line, $replace)
    {
        // count number of lines
        $lines = explode("\n", $content);

        $line_number = $line - 2;
        // extract the line_number for class
        $segment_1 = array_slice($lines, 0, $line_number);

        // extract the last -1 line of ocde
        $segment_2 = array_slice($lines, $line_number, count($lines));

        // add before the segment1 the new code
        array_push($segment_1, $replace);

        // put together
        $new_content = array_merge($segment_1, $segment_2);
        return $str = implode("\n", $new_content);
    }

    /**
     * -------------------------------------------------------------------------
     * remove line findnt the reference
     * -------------------------------------------------------------------------
     * @param type $content
     * @param type $replace
     * @return type
     */
    public static function replaceLineReference($content, $reference, $replace)
    {
        // count number of lines
        $lines = explode("\n", $content);
        $lines_content = array_map('trim', $lines);

        $position_to_break = array_search($reference, $lines_content);
        if ($position_to_break != false) {
            $line_number = $position_to_break;

            unset($lines[$line_number]);

            // extract the line_number for class
            $segment_1 = array_slice($lines, 0, $line_number);

            // extract the last -1 line of ocde
            $segment_2 = array_slice($lines, $line_number, count($lines) - 1);

            // add before the segment1 the new code
            array_push($segment_1, $replace);

            // put together
            $new_content = array_merge($segment_1, $segment_2);
            return $str = implode("\n", $new_content);
        } else {
            die('reference ' . $reference . ' not found in replaceLineReference!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * remplace content of line in content with replace
     * -------------------------------------------------------------------------
     * @param type $content
     * @return type
     */
    public static function replaceLineInFile($content, $line, $replace)
    {
        // count number of lines
        $lines = explode("\n", $content);

        $line_number = $line - 1;
        unset($lines[$line_number]);
        // extract the line_number for class
        $segment_1 = array_slice($lines, 0, $line_number);

        // extract the last -1 line of ocde
        $segment_2 = array_slice($lines, $line_number, count($lines));

        // add before the segment1 the new code
        array_push($segment_1, $replace);

        // put together
        $new_content = array_merge($segment_1, $segment_2);
        return $str = implode("\n", $new_content);
    }

    /**
     * -------------------------------------------------------------------------
     * Get folders from $base_bat
     * -------------------------------------------------------------------------
     * @param type $base_path
     * @param type $callback
     * @return type
     */
    public static function getFolders($base_path, $callback = 'basename')
    {

        return $directories = array_map($callback, glob($base_path . '*', GLOB_ONLYDIR));
    }

    /**
     * -------------------------------------------------------------------------
     * Delete recursive filder
     * -------------------------------------------------------------------------
     * @param type $src
     */
    public static function deleteFolder(string $src)
    {
        if(empty($src)){
            die("empty dir");
        }
        $dir = opendir($src);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    self::deleteFolder($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }

    /**
     * -------------------------------------------------------------------------
     * extract string between 2 delimiters
     * -------------------------------------------------------------------------
     * @param string $str
     * @param string $start
     * @param string $end
     * @return string
     */
    public static function extractFromString($str, $start, $end)
    {
        $string = ' ' . $str;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

}
