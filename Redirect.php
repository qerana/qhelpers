<?php

/*
 * This file is part of qeranaProject
 * Copyright (C) 2020-2021  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

/**
 * -----------------------------------------------------------------------------
 * RedirectClass
 * -----------------------------------------------------------------------------
 */
class Redirect {

    /**
     * -------------------------------------------------------------------------
     * Go to home
     * -------------------------------------------------------------------------
     */
    public static function home() {
        header('location: ' . __URL__);
        die();
    }

    /**
     * -------------------------------------------------------------------------
     * got to url
     * -------------------------------------------------------------------------
     * @param type $path
     */
    public static function to($path) {
        header('location: ' . __URL__ . $path);
        die();
    }

    /**
     * Redirect to current model/controller and method
     * @param type $method
     */
    public static function toAction($method) {
        $url = __URL__ . '/' . \helpers\Url::getModule() . '/' . \helpers\Url::getController() . '/' . $method;
        header('location: ' . $url);
        die();
    }

}
